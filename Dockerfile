FROM amd64/ubuntu:latest

# This is a WIP 🚧
# docker build -t galago-test . 
# I want to expose the container’s port 8080 (standard http port) 
# on the host’s port 8888.
# docker run -d -p 8888:8080 galago-test

USER root
WORKDIR /home

COPY . /home/

RUN apt-get update && \
    apt-get -y install curl gnupg git wget redis-server && \
    curl -sL https://deb.nodesource.com/setup_15.x  | bash - && \
    apt-get -y install nodejs && \
    npm install pm2 -g && \
    wget https://golang.org/dl/go1.16.8.linux-amd64.tar.gz && \ 
    tar -C /usr/local -xzf go1.16.8.linux-amd64.tar.gz && \ 
    export PATH=$PATH:/usr/local/go/bin && \
    rm go1.16.8.linux-amd64.tar.gz && \
    go version && \
    wget https://github.com/tinygo-org/tinygo/releases/download/v0.19.0/tinygo_0.19.0_amd64.deb && \
    dpkg -i tinygo_0.19.0_amd64.deb && \
    rm tinygo_0.19.0_amd64.deb && \
    export PATH=$PATH:/usr/local/tinygo/bin && \
    tinygo version && \
    npm install

ENV PATH "$PATH:/usr/local/go/bin"
ENV PATH "$PATH:/usr/local/tinygo/bin"

ENTRYPOINT ./galago.docker.start.sh

/* [DOC]
## galago.config.js

This data are readable from any function

```golang
settings := js.Global().Call("getConfig", []interface{}{})
javaScriptObject := settings.JSValue()
println(javaScriptObject.Get("name"))
println(javaScriptObject.Get("version"))
```

[DOC] */
exports.config = {
  version: "poc",
  name: "bush baby"
}

package main

import (
	"syscall/js"
)

func Handle(_ js.Value, args []js.Value) interface{} {
	// gat the object parameters
	parameters := args[0]
	// get members of an object
	a := parameters.Get("a").Int()
	b := parameters.Get("b").Int()

	return a+b
}

func main() {
	println("🤖: add wasm loaded")

	js.Global().Set("Handle", js.FuncOf(Handle))

	<-make(chan bool)
}

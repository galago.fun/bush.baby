package main

import (
	"syscall/js"
)

func Handle(_ js.Value, args []js.Value) interface{} {
	name := args[0].Get("name")

	return map[string]interface{}{
		"message": "👋 Hey " + name.String(),
	}
}

func main() {
	println("🤖: hey wasm function loaded - go version")

	js.Global().Set("Handle", js.FuncOf(Handle))

	// https://github.com/golang/go/issues/29845
	go func(){ 
		js.Global().Call("startCb")
	}()

	<-make(chan bool)
}

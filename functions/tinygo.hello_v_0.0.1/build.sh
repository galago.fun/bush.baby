#!/bin/bash
tinygo build -o tinygo.hello.wasm -target wasm ./main.go
tinygo build -o ../../wasm.functions/tinygo.hello_v_0.0.1.wasm -target wasm ./main.go
ls -lh *.wasm
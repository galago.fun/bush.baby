const initializeWasm = require("../../helpers/wasm.loaders/tinygo.wasm.js.loader").initializeWasm

const fs = require("fs")

const wasmFile = fs.readFileSync("./tinygo.hello.wasm")

initializeWasm(wasmFile).then(result => {
  console.log(Handle({
    firstName: "Bob",
    lastName: "Morane"
  }))
})

initializeWasm(wasmFile).then(result => {
  console.log(Handle({
    firstName: "Jane",
    lastName: "Doe"
  }))
})


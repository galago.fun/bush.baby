const initializeWasm = require("../../helpers/wasm.loaders/go.wasm.js.loader").initializeWasm

const fs = require("fs")

const wasmFile = fs.readFileSync("./go.hello.wasm")

global.startCb = () => {
  console.log(Handle({
    firstName: "Bob",
    lastName: "Morane"
  }))
  
}

initializeWasm(wasmFile).then(result => {})



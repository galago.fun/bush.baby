package main

import (
	"syscall/js"
)

func Html(_ js.Value, args []js.Value) interface{} {
	return `
		<h1>🌕 Hello World 🚀</h1>
		<h2>GoLang is amazing</h2>
		<h3>Wasm is fantastic</h3>
	`
}

func main() {
	js.Global().Set("Html", js.FuncOf(Html))

	// https://github.com/golang/go/issues/29845
	go func() {
		js.Global().Call("startCb")
	}()

	<-make(chan bool)
}

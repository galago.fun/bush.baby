package main

/*
./galagoctl build ./golang.samples/demo.webpage.go ./webpage.wasm
./galagoctl deploy webpage ./webpage.wasm 1.1.x
./galagoctl activate webpage 1.1.x
call functions/web/html/webpage
*/

import (
	"syscall/js"
)

func Html(_ js.Value, args []js.Value) interface{} {
	return `
	<!doctype html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="x-ua-compatible" content="ie=edge">
			<title>Hello World!</title>
			<meta name="description" content="">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="../../../../functions/web/css/webpage">
			<!--
			<style>
			</style>
			-->
			<script src="../../../../functions/web/javascript/webpage"></script>
		</head>
		<body>
			<section class="container">
				<div>
					<h1 class="title">
					👋 Hello World 🌍
					</h1>
					<h2 class="subtitle">
					made with 💚 🍵 and GalaGo
					</h2>              
				</div>
			</section>
			<script>

				fetch('../../../../functions/call/webpage', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'ADMIN_GALAGO_TOKEN': 'ILOVEPANDA' // use an authentication step before
					},
					body: JSON.stringify({})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
				})
				.catch(error => {
				 console.log(error)
				})

				fetch('../../../../functions/web/plain/webpage')
				.then(res => res.text())
				.then(data => {
					console.log(data)
				})
				.catch(error => {
				 console.log(error)
				})

			</script>
		</body>
	</html>  	
	`
}

func Css(_ js.Value, args []js.Value) interface{} {
	return `
	.container { min-height: 100vh; display: flex; justify-content: center; align-items: center; text-align: center; }
	.title { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; display: block; font-weight: 300; font-size: 100px; color: #35495e; letter-spacing: 1px; }
	.subtitle { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; font-weight: 300; font-size: 42px; color: #526488; word-spacing: 5px; padding-bottom: 15px; }
	.links { padding-top: 15px; }
	`
}

func Plain(_ js.Value, args []js.Value) interface{} {
	return `🖐 this is a text sample 🙂`
}

func JavaScript(_ js.Value, args []js.Value) interface{} {
	return `console.log("👋 Hello World 🌍")`
}

// POST
func Handle(_ js.Value, args []js.Value) interface{} {
	return map[string]interface{}{
		"author":  "@k33g 🐼",
		"message": "hello",
	}
}

func main() {

	js.Global().Set("Handle", js.FuncOf(Handle))
	js.Global().Set("Html", js.FuncOf(Html))
	js.Global().Set("Css", js.FuncOf(Css))
	js.Global().Set("Plain", js.FuncOf(Plain))
	js.Global().Set("JavaScript", js.FuncOf(JavaScript))

	<-make(chan bool)
}

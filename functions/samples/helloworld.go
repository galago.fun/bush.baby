package main

import (
	"syscall/js"
)

func Handle(_ js.Value, args []js.Value) interface{} {
	return "👋 Hello World 🌍🌍🐼" 
}

func main() {
  
	js.Global().Set("Handle", js.FuncOf(Handle))
	
	<-make(chan bool)
}

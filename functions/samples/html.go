package main

import (
	"syscall/js"
)


func Html(_ js.Value, args []js.Value) interface{} {
	return `
		<h1>🎃 Hello World 🎃</h1>
		<h2>Tiny GoLang is amazing</h2>
	`
}

func Handle(_ js.Value, args []js.Value) interface{} {
	return `
		<h1>👋 Hello World 🌍</h1>
		<h2>Tiny GoLang is amazing</h2>
	`
}

func main() {
  
	js.Global().Set("Handle", js.FuncOf(Handle))
	js.Global().Set("Html", js.FuncOf(Html))
	<-make(chan bool)
}

package main

import (
	"syscall/js"
)

func Handle(_ js.Value, args []js.Value) interface{} {
	// get an object
	jsonPayload := args[0]
	// get members of an object
	name := jsonPayload.Get("name").String()

	requestHeaders := args[1]
	// headers fields neam always in minus case
	demoToken := requestHeaders.Get("demo_token").String()

	return map[string]interface{}{
		"message": "👋 Hello " + name,
		"token":   demoToken,
		"author":  "@k33g_org 🐼",
	}
}

func main() {
	println("🤖: hi wasm loaded")

	js.Global().Set("Handle", js.FuncOf(Handle))

	<-make(chan bool)
}

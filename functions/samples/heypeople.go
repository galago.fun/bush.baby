package main

import (
	"syscall/js"
)

func Handle(_ js.Value, args []js.Value) interface{} {
	sum := 0
	for {
			sum++ // repeated forever
	}
}

func main() {
	js.Global().Set("Handle", js.FuncOf(Handle))
	<-make(chan bool)
}

package main

import (
	"syscall/js"
)

/*
🖐 if you scale the function you'll create a new MQTT client, because it's a new process
*/

func Handle(_ js.Value, args []js.Value) interface{} {
	println("🌈 executing mqtt function")

	js.Global().Call("mqttPublish", "home", "🌺 Mqtt message from GoLang 🌈")

	return "message 🚀"
}

func main() {
	println("🤩 I ❤️ GalaGo")
	js.Global().Set("Handle", js.FuncOf(Handle))

	subscribeCb := func(this js.Value, args []js.Value) interface{} {
		error := args[0].JSValue()
		if error.IsNull() {
			println("😍 subscribe to home topic")
		}
		return ""
	}

	onMessageCb := func(this js.Value, args []js.Value) interface{} {
		topic := args[0].String()
		message := args[1].JSValue().String()
		println("🖖 " + message + " on: " + topic)
		return ""
	}

	connectedCb := func(this js.Value, args []js.Value) interface{} {
		functionName := args[0].String()
		println("🚀 mqtt client for " + functionName + ".wasm is connected")
		return ""
	}

	js.Global().Call("mqttInitialize", js.FuncOf(connectedCb))

	js.Global().Call("mqttSubscribe", "home", js.FuncOf(subscribeCb))

	js.Global().Call("mqttOnMessage", js.FuncOf(onMessageCb))

	<-make(chan bool)
}

package main

import (
	"syscall/js"
)


func Handle(_ js.Value, args []js.Value) interface{} {
	println("🌈 executing settings function")

	settings := js.Global().Call("getConfig", []interface{}{})
	javaScriptObject := settings.JSValue()

	galagoVersion := js.Global().Call("getEnvironmentVariable", "GALAGO_VER").JSValue().String()

	galagoToken := js.Global().Call("getEnvironmentVariable", "ADMIN_GALAGO_TOKEN").JSValue().String()

	return map[string]interface{}{
		"author":         "@k33g 🐼",
		"projectName":    javaScriptObject.Get("name"),
		"projectVersion": javaScriptObject.Get("version"),
		"galagoVersion":  galagoVersion,
		"galagoToken":    galagoToken,
	}
}

func main() {
	println("🤩 I ❤️ GalaGo")
	js.Global().Set("Handle", js.FuncOf(Handle))
	<-make(chan bool)
}

package main

import (
	"syscall/js"
)

func Handle(_ js.Value, args []js.Value) interface{} {
	// get an object
	human := args[0]
	// get members of an object
	firstName := human.Get("firstName")
	lastName := human.Get("lastName")

	return map[string]interface{}{
		"message": "👋 Hello " + firstName.String() + " " + lastName.String(),
		"author":  "@k33g_org 🐼",
		"version": "000 🌺",
	}
}

func main() {
	println("🤖: hello[000] 🌺 wasm loaded")

	js.Global().Set("Handle", js.FuncOf(Handle))

	<-make(chan bool)
}

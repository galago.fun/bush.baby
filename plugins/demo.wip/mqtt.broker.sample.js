const aedes = require('aedes')()
const server = require('net').createServer(aedes.handle)

server.listen(process.env.MQTT_PORT || 1883,  _ => {
  console.log('server started and listening on port ', process.env.MQTT_PORT || 1883)
})

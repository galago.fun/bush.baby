const mqtt = require('mqtt')
let client  = mqtt.connect(process.env["MQTT_ADDR"] || "mqtt://localhost:1883")

client.on("connect", _ => {
  client.subscribe("home", error => {
    if (!error) {
      client.publish("home", "👋 Hello World 🌍")
    }
  })
})

client.on("message", (topic, message) => {
  // message is Buffer
  console.log(topic, "➡️", message.toString())
  client.end()
})
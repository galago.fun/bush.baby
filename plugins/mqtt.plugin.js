/* [DOC]
## MQTT Plugin

> 🚧 This is a work in progress (== things may be change)

[DOC] */

// TODO: authentication, certs, ...

const mqtt = require('mqtt')

function plugin(global) {
  
  global.wasmMQTTClient = null

/* [DOC]
### function mqttInitialize

[DOC] */
  function mqttInitialize(callback) {
    // Create the MQTT client
    global.wasmMQTTClient = mqtt.connect(`${process.env["MQTT_ADDR"]}`)
    global.wasmMQTTClient.on("connect", _ => {
      console.log(`📧> wasm mqtt client for ${global.wasmFunctionName} connected`)
      callback(global.wasmFunctionName)
    })
  }
  global.mqttInitialize = mqttInitialize

/* [DOC]
### function mqttSubscribe

[DOC] */
  function mqttSubscribe(topic, callback) {
    global.wasmMQTTClient.subscribe(topic, (error) => {
      callback(error)
    })
  }
  global.mqttSubscribe = mqttSubscribe

/* [DOC]
### function mqttOnMessage

[DOC] */
  function mqttOnMessage(callback) {
    global.wasmMQTTClient.on("message", (topic, message) => {
      callback(topic, message.toString())
    })
  }
  global.mqttOnMessage = mqttOnMessage

/* [DOC]
### function mqttPublish

[DOC] */
  function mqttPublish(topic, message) {
    global.wasmMQTTClient.publish(topic, message)
  }
  global.mqttPublish = mqttPublish

}



module.exports = {
  plugin
}
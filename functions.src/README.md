This directory is used to store the source code of the function when using the API (with a curl or with the GalaGo CLI) to deploy source code t be build on the server side by the GalaGo application:

```bash
url_api="http://localhost:8080"
galago_token="${ADMIN_GALAGO_TOKEN}"
function_name="helloworld"
source_code="./demo.helloworld.go"
function_version="0.0.9"
curl -F "${function_name}=@${source_code}" \
      -H "Content-Type: multipart/form-data" \
      -H "ADMIN_GALAGO_TOKEN: ${galago_token}" \
      -X POST ${url_api}/functions/build/${function_version}
```

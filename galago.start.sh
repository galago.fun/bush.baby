#!/bin/bash
export GALAGO_VER="🥚.🐣.🐥"
export REDIS_ADDR="localhost"
export REDIS_PORT="6379"
export MQTT_ADDR="mqtt://localhost:1883"
export HTTP_PORT="8080"
export ADMIN_GALAGO_TOKEN="ILOVEPANDA"
export GALAGO_FUNCTIONS_PATH="./wasm.functions"
export GALAGO_FUNCTIONS_CODE="./functions.src"
export GALAGO_FUNCTIONS_STATES_PATH="./wasm.executors.versions.states.json"
#export GALGO_CERT="app.galago.fun.crt"
#export GALGO_KEY="app.galago.fun.key"
#export HTTPS_PORT="3000"
redis-server --daemonize yes

pm2 start mqtt.broker.js
pm2 start galago.server.js


// executors' versions' states
const fs = require('fs')

/* [DOC]
## class WasmExecutorsVersionsStates

> 🖐 temporary implementation

[DOC] */

class WasmExecutorsVersionsStates {

  file = process.env.GALAGO_FUNCTIONS_STATES_PATH || "./wasm.executors.versions.states.json"
  states = null
  timer = null

  constructor() {
    try {
      this.states = JSON.parse(fs.readFileSync(this.file, {encoding: "utf8"}))
    } catch(error) {
      console.log("😡 when the states file", error)
      this.states = {}
      //process.exit(1)
    } finally {
      let save = () => {
        let data = JSON.stringify(this.states, null, 2)      
        fs.writeFile(this.file, data, {encoding: "utf8"}, error => {
          if (error) {
            console.error("😡", error)
            return
          }
          //file written successfully
        })
      }
      this.timer = setInterval(save, 3000)
    }
  }
}

module.exports = {
  WasmExecutorsVersionsStates
}
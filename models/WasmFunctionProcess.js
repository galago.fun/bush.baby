const fork = require('child_process').fork

/* [DOC]
## class WasmFunctionData


[DOC] */
const WasmFunctionData = require('./WasmFunctionData').WasmFunctionData

class WasmFunctionProcess {
  process = fork("./helpers/wasm.child.process.js")
  wasmFunction = new WasmFunctionData()
  counter
  constructor() {
    this.counter = 0
  }
}

module.exports = {
  WasmFunctionProcess
}
/* [DOC]
## wasm.functions.js


[DOC] */

function getList({executorsCollection}) {

  let data = []

  executorsCollection.forEach(executorEntry => {

    //let wasmFileName = executorEntry[0]
    let executor = executorEntry[1]
    
    let functionDescription = `${executor.name}[${executor.version}](${executor.isDefaultVersion==true ? "*" : "-"})`

    let relatedProcessesList = executor.functionProcesses
    
    relatedProcessesList.forEach((item, index) => {
      let process = item.process

      let wasmFunction  = item.wasmFunction
      let counter = item.counter

      let started = wasmFunction.started != null 
        ? `${wasmFunction.started.toLocaleDateString()} ${wasmFunction.started.getHours()}:${wasmFunction.started.getMinutes()}:${wasmFunction.started.getSeconds()}:${wasmFunction.started.getMilliseconds()}`
        : ""

      var ended = wasmFunction.ended != null 
        ? `${wasmFunction.ended.toLocaleDateString()} ${wasmFunction.ended.getHours()}:${wasmFunction.ended.getMinutes()}:${wasmFunction.ended.getSeconds()}:${wasmFunction.ended.getMilliseconds()}`
        : ""        
      
      var duration = wasmFunction.ended == null && wasmFunction.started != null
        ? new Date() - wasmFunction.started
        : wasmFunction.duration != null ? wasmFunction.duration : 0
      

      if(process.signalCode==="SIGTERM") { // it happends when you do a `kill pid` (unix way)
        wasmFunction.status = "😵 killed"
        duration = ""
      }

      if(duration>5000) {
        ended = "🔥"
      }

      // ["function", "index", "pid", "started", "ended", "duration", "status", "counter"]
      //  let functionDescription = `${executor.name}[${executor.version}](${executor.isDefaultVersion==true ? "*" : "-"})`

      data.push({
        function: executor.name,
        version: executor.version,
        isDefaultVersion: executor.isDefaultVersion,
        description: functionDescription,
        processIndex: index,
        pid: process.pid,
        started: started,
        ended: ended,
        duration: duration,
        status: wasmFunction.status,
        counter: counter
      })  

    })
    
  })

  return data

}

module.exports = {
  getList
}
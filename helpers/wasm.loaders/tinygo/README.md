🖐 `wasm_exec.js` must be updated when TinyGo is upgraded (or installed)

### Get the `wasm_exec.js` file

```bash
wget https://raw.githubusercontent.com/tinygo-org/tinygo/v0.19.0/targets/wasm_exec.js
```
/* [DOC]
## initializeWasm

This method runs the main function of a GoLang wasm file.

> - **Prerequisites**: load the wasm file `wasmFile = fs.readFileSync(wasmFilePath)`
> - **Remarks**:
>   - this helper is developped to work with nodejs
>   - usage of `WebAssembly.instantiate` instead of `WebAssembly.instantiateStreaming` (for the browser)

[DOC] */

require("./go/wasm_exec")

global.startCb = () => {
  console.log("🚀🚀🚀 initialize and load to global context the Go function")
  //global.goFuncLoaded = true
}

function initializeWasm(wasmFile, args, options) {
  const go = new Go()
  // hack for tiny go
  // go.importObject.env["syscall/js.finalizeRef"] = () => {}

  // useful? (TODO: to be checked)
  if(options) go.importObject.env = options

  return new Promise((resolve, reject) => {
    // instantiate: nodejs
    // instantiateStreaming: browser
    WebAssembly.instantiate(wasmFile, go.importObject)
    .then(result => {
      if(args) go.argv = args // node supported by the wasm_exec.js version of TinyGO
      go.run(result.instance) 
      resolve(result.instance)
    })
    .catch(error => {
      reject(error)
    })
  })
}

exports.initializeWasm = initializeWasm
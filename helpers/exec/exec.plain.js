function execPlain(message, process) {
  try {
    let result = Plain(message.funtionParameters, message.functionHeaders)
    process.send({success: result})
  } catch(error) {
    process.send({failure: error})
    throw error // 🤔
  }
}

exports.execPlain = execPlain
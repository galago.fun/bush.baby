function execJavaScript(message, process) {
  try {
    let result = JavaScript(message.funtionParameters, message.functionHeaders)
    process.send({success: result})
  } catch(error) {
    process.send({failure: error})
    throw error // 🤔
  }
}

exports.execJavaScript = execJavaScript
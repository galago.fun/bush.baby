function execFunction(message, process) {
  try {
    console.log("🚀", "executing the Handle function")
    let result = Handle(message.funtionParameters, message.functionHeaders)
    process.send({success: result})
  } catch(error) {
    console.log("😡", error)
    process.send({failure: error})
    throw error // 🤔
  }
}

exports.execFunction = execFunction
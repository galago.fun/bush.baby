function execHtml(message, process) {
  try {
    let result = Html(message.funtionParameters, message.functionHeaders)
    process.send({success: result})
  } catch(error) {
    process.send({failure: error})
    throw error // 🤔
  }
}

exports.execHtml = execHtml
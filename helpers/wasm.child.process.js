/* [DOC]
## Wasm Child Process

A wasm child process is a nodejs process with to main roles(tasks/functions). Once the process is created, you can send it 2 kinds of commands (the command is embedded in the message):

- `message.cmd == "load"` *load the wasm fil in memory*
- `message.cmd == "exec"` *execute the `Handle`function of the Go program*

> Ref: https://nodejs.org/api/child_process.html

### Structure and roles of the messages

#### load use case

- the process will load the wasm file and run the `main` method of the Go program (then the `Handle` method is loaded in the global scope/context of the child process **only**)
- the `wasmFunction` variable is setted with the `Handle` method
- then the process send a reply message: `{success: "loaded"}` (or `{failure: error}`)

> structure of the **"load message"**
```javascript
{
  cmd: "load", 
  wasmFilePath: <the_path_of_the_wasm_file>,
  functionName: <the_name_of_the_function>
}
```
> the **"load message"** is sent in:
> - `index.js` at start, for each load of a wasm file
> - `routes.wasm.build.js` once a Go source code is built and deployed
> - `routes.wasm.publish.js` once a wasm file is deployed
> - `routes.wasm.scale.js` once a new child process is created

#### exec usecase

- the process executes the `Handle` function (from the Go program) (it calls the function with the parameters):
  - `wasmFunction(message.funtionParameters)`
- then the process send a reply message: `{success: "loaded"}` (or `{failure: error}`)

> structure of the **"exec message"**
```javascript
{
  cmd: "exec",
  funtionParameters: <function_parameters>
}
```

> the **"exec message"** is sent in:
> - `routes.wasm.functions.js`

### Get information "from outside"

It's possible to provide "external" information to the Go program thanks to 2 JavaScript functions:

- `getEnvironmentVariable`: retrieve the value of a global (1) environment variable by its name
- `getConfig`: retrieve a set of data from a config file (2)

> - (1): the environment variables are the same for all the wasm functions
> - (2): the config file is unique and the same for all the wasm functions

### How to use `getEnvironmentVariable` (from Go)

```golang
galagoVersion := js.Global().Call("getEnvironmentVariable", "GALAGO_VER").JSValue().String()
```

### How to use `getConfig` (from Go)

```golang
settings := js.Global().Call("getConfig", []interface{}{})
javaScriptObject := settings.JSValue()
projectName := javaScriptObject.Get("name")
projectVersion := javaScriptObject.Get("version")
```
[DOC] */

// used by:
// - WasmFunctionProcess.js

const fs = require('fs')

const execFunction = require("./exec/exec.function").execFunction
const execHtml = require("./exec/exec.html").execHtml
const execCss = require("./exec/exec.css").execCss
const execPlain = require("./exec/exec.plain").execPlain
const execJavaScript = require("./exec/exec.javascript").execJavaScript


const implantPlugins = require("../plugins/plugins.process").implantPlugins


global.wasmFunction = null
global.wasmFunctionName = null

// plugins location: `../plugins`
implantPlugins(global)

function includeLoader(compiler) {
  
  switch (compiler) {
    case "go":
      console.log("🖐️ this is a Go function")
      return require("./wasm.loaders/go.wasm.js.loader").initializeWasm
      break;
    case "tinygo":
      console.log("🖐️ this is a 😍 TinyGo function")
      return require("./wasm.loaders/tinygo.wasm.js.loader").initializeWasm
      break;
    default:
      return null
      break;
  }
}

function loadWasmFile(message, process) {
  console.log("📝 structure:", message)
    /*
      📝 structure: {
        cmd: 'load',
        wasmFilePath: './wasm.functions/tinygo.index_v_0.0.0.wasm',
        compiler: 'tinygo',
        functionName: 'tinygo.index_v_0.0.0',
        wasmFileName: 'tinygo.index_v_0.0.0.wasm'
      }
    */
  let initializeWasm = includeLoader(message.compiler)

  // Load the wasm file
  fs.readFile(message.wasmFilePath, (error, wasmFile) => {
    if(error) {
      console.log("😡", error)
      process.send({failure: error})
      throw error // 🤔
    } else {
      initializeWasm(wasmFile).then(() => {
        
        global.wasmFunctionName = message.functionName
        console.log("📦", message.wasmFilePath, "loaded")
        process.send({success: "loaded"})
      })
      .catch(error => {
        console.log("😡", error)
        process.send({failure: error})
        throw error // 🤔
      })
    }
  })

}

process.on("message", async (message) => {

  console.log(`🤖> message received from parent process:`, message)

  switch (message.cmd) {
    case "load":
      loadWasmFile(message, process)
      break;
    
    case "exec": // it comes from a POST request
      execFunction(message, process)
      break;
      
    case "exec_html": // it comes from a GET request
      execHtml(message, process)
      break;

    case "exec_css": // it comes from a GET request
      execCss(message, process)
      break;

    case "exec_Plain": // it comes from a GET request
      execPlain(message, process)
      break;
    
    case "exec_javascript": // it comes from a GET request
      execJavaScript(message, process)
      break;

    default:
      break;
  }

})

// TODO: check if this is tiggered when the process is killed
process.on("exit", (code) => {
  console.log(`🤖> exit code:`, code)
})

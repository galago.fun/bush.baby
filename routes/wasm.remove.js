/* [DOC]
## wasm.remove.js

- `/functions/remove`

[DOC] */

const fs = require('fs')

async function remove (fastify, options) {

  // route protection
  // see galago.start.sh
  fastify.addHook('onRequest', async (request, reply) => {
    let token = request.headers["admin_galago_token"]
    if (options.adminGalagoToken==="" || options.adminGalagoToken===token) {
      // all good
    } else {
      reply.code(401).send({
        failure: "😡 Unauthorized",
        success: null
      })
    }
  })

/* [DOC]
### route: `/functions/remove`

[DOC] */
  fastify.post(`/functions/remove/:function_name/:function_version`, async (request, reply) => {
    let functionName = request.params.function_name
    // executor version == function version
    let executorVersion = request.params.function_version
    // check the name
    let wasmFileName = `${functionName}_v_${executorVersion}.wasm`

    // ref: `models/WasmExecutor.js`
    let wasmExecutorEntry = options.wasmExecutorsMap.get(wasmFileName) 

    if(wasmExecutorEntry) { // it exists
      wasmExecutorEntry.deActivateAsDefaultVersion()
      // update the state of the active running versions
      delete options.wasmExecutorsVersionsStates.states[functionName]
    /*
      - kill the processes
      - remove the entry
      - remove the wasm file
    */
      let killingResult = wasmExecutorEntry.killAllWasmFunctionProcesses()

      if (killingResult.failure) {
        // something wrong when killing processes
        //reply.code(500).send(killingResult.failure)
        reply.code(500).send(killingResult)
      } else {
        // allgood
        // remove file and wasmExecutorEntry
        wasmExecutorEntry.deleteWasmFile().then(deletingResult => {
          options.wasmExecutorsMap.delete(wasmFileName) 
          //reply.code(200).send(deletingResult.success)
          reply.code(200).send(deletingResult)
        }).catch(deletingResult => {
          //reply.code(500).send(deletingResult.failure)
          reply.code(500).send(deletingResult)
        })
        
      } //end if
    } else {
      reply.code(500).send({
        failure: `😡 ${functionName}_v_${executorVersion} doesn't exist`,
        success: null
      })
    }


  })

}

module.exports = remove

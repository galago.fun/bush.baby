## wasm.functions.js

execute a function
POST /functions/call/:function_name

execute a version of function
POST /functions/call/:function_name/:function_version

activate the default version of a function
POST /functions/activate/:function_name/:function_version

## wasm.publish.js

publish a wasm file
POST /functions/publish/:version

## wasm.scale.js

create a new process with the same function
add the process to the wasm executor
POST /functions/scale/:function_name/:function_version

## wasm.child.processes.js

Get the list of executors and details
GET /functions/processes/list

kill a process of a function version
POST /functions/kill/:function_name/:function_version/:process_index

Get the functions table
GET /functions/processes/table

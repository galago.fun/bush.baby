/* [DOC]
## wasm.index.functions.js

🖐 the route(s) isn't/aren't protected by a token

[DOC] */

function execute(type, functionName, defaultWasmExecutorVersion, request, reply, options) {

  let wasmExecutorEntry = options.wasmExecutorsMap.get(`${functionName}_v_${defaultWasmExecutorVersion}.wasm`) 

  if(wasmExecutorEntry) { // it exists


    let wasmFunctionProcess = wasmExecutorEntry.giveMeTheMostAppropriateFunctionProcess()

    let wasmChildProcess = wasmFunctionProcess.process
    let relatedWasmFunction = wasmFunctionProcess.wasmFunction
  
    let contentType = `text/${type}; charset=utf-8`
    
    // 🖐 once method is better than on method
    wasmChildProcess.once("message", (message) => { 
  
      relatedWasmFunction.ended = new Date()
      relatedWasmFunction.duration = relatedWasmFunction.ended - relatedWasmFunction.started
  
      if(message.success) {
  
        relatedWasmFunction.status = "🟢 success"
        // metrics
        wasmFunctionProcess.counter+=1

        // 🖐️ send the result of the function
        reply.header('Content-Type', contentType).send(message.success) 
  
        //reply.send(message.success) 
  
      } else {
        relatedWasmFunction.status = "🔴 failure"
        if(message.failure) {
          reply.code(500).send({failure: message.failure})
        } else {
          reply.code(500).send({failure: "unknown error"})
        }
      }
    })
  
    relatedWasmFunction.status = "🚀 running"
    relatedWasmFunction.started = new Date()
    relatedWasmFunction.ended = null
  
    wasmChildProcess.send({
      cmd: `exec_${type}`,
      funtionParameters: request.params, // 🤔
      functionHeaders: request.headers
    })

  } else {
    reply.code(503).send({failure: `${functionName}_v_${defaultWasmExecutorVersion} doesn't exist`})
  }


  
}


async function wasmWebFunctions (fastify, options) {

/* [DOC]
### route `/functions/web/:type/:function_name/:function_version`

Possible values for type: `html, javascript, css, plain`

[DOC] */
  fastify.get(`/functions/web/:type/:function_name/:function_version`, async (request, reply) => {

    let type = request.params.type
    let functionName = request.params.function_name
    // executor version == function version
    let defaultWasmExecutorVersion = request.params.function_version
    execute(type, functionName, defaultWasmExecutorVersion, request, reply, options)

    await reply

  })

/* [DOC]
### route `/functions/web/:type/:function_name`

Possible values for type: `html, javascript, css, plain`

[DOC] */

  fastify.get(`/functions/web/:type/:function_name`, async (request, reply) => {
    let type = request.params.type
    let functionName = request.params.function_name
    let defaultWasmExecutorVersion = options.wasmExecutorsVersionsStates.states[functionName]

    execute(type, functionName, defaultWasmExecutorVersion, request, reply, options)

    await reply
    
  })

}

module.exports = wasmWebFunctions

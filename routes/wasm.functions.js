/* [DOC]
## wasm.functions.js

- `/functions/call/:function_name/:function_version`
- `/functions/call/:function_name`

[DOC] */

function execute(functionName, jsonParameters, defaultWasmExecutorVersion, request, reply, options) {

  let wasmExecutorEntry = options.wasmExecutorsMap.get(`${functionName}_v_${defaultWasmExecutorVersion}.wasm`)

  if(wasmExecutorEntry) { // it exists
    let wasmFunctionProcess = wasmExecutorEntry.giveMeTheMostAppropriateFunctionProcess()

    let wasmChildProcess = wasmFunctionProcess.process
    let relatedWasmFunction = wasmFunctionProcess.wasmFunction
    
    // 🖐 once method is better than on method
    // TODO: force the content type? (json?)
    wasmChildProcess.once("message", (message) => { 
  
      relatedWasmFunction.ended = new Date()
      relatedWasmFunction.duration = relatedWasmFunction.ended - relatedWasmFunction.started
  
      if(message.success) {
        relatedWasmFunction.status = "🟢 success"
        // metrics
        wasmFunctionProcess.counter+=1
  
        // 🖐️ send the result of the function
        reply.header('Content-Type', 'application/json; charset=utf-8').send(message.success) 
        //reply.send(message.success) 
  
      } else {
        relatedWasmFunction.status = "🔴 failure"
        if(message.failure) {
          reply.code(500).send({failure: message.failure})
        } else {
          reply.code(500).send({failure: "unknown error"})
        }
      }
    })
  
    relatedWasmFunction.status = "🚀 running"
    relatedWasmFunction.started = new Date()
    relatedWasmFunction.ended = null

    console.log("🍎", request.headers)
  
    wasmChildProcess.send({
      cmd: "exec",
      funtionParameters: jsonParameters, // posted json payload
      functionHeaders: request.headers
    })

  } else {
    reply.code(503).send({failure: `${functionName}_v_${defaultWasmExecutorVersion} doesn't exist`})
  }


}

async function wasmFunctions (fastify, options) {

/* [DOC]
### route `/functions/:function_name/:function_version`

This route allows to call a specific version of a function

> How to use it
```bash
function_name="hello"
function_version="first"
data='{"name":"Bob"}'
curl -d "${data}" \
      -H "Content-Type: application/json" \
      -X POST "${url_api}/functions/call/${function_name}/${function_version}"
``` 

[DOC] */
  fastify.post(`/functions/call/:function_name/:function_version`, async (request, reply) => {
    let jsonParameters = request.body
    let functionName = request.params.function_name
    // executor version == function version
    let defaultWasmExecutorVersion = request.params.function_version

    execute(functionName, jsonParameters, defaultWasmExecutorVersion, request, reply, options)
    
    await reply
  })

/* [DOC]
### route `/functions/:function_name`

This route allows to call the **active** version of a function


> How to use it
```bash
data='{"name":"Bob"}'
function_name="hello"
curl -d "${data}" \
      -H "Content-Type: application/json" \
      -X POST "${url_api}/functions/call/${function_name}"
``` 

[DOC] */
  fastify.post(`/functions/call/:function_name`, async (request, reply) => {

    let jsonParameters = request.body
    let functionName = request.params.function_name

    let defaultWasmExecutorVersion = options.wasmExecutorsVersionsStates.states[functionName]

    execute(functionName, jsonParameters, defaultWasmExecutorVersion, request, reply, options)
    
    await reply
    
  })
}

module.exports = wasmFunctions

/* [DOC]
## wasm.scale.js

- `/functions/scale/:function_name/:function_version`

[DOC] */

const addNewWasmFunctionProcessToWasmExecutor = require('../helpers/wasm.executor').addNewWasmFunctionProcessToWasmExecutor

async function scale (fastify, options) {

  // route protection
  // see galagoctl.config
  // see galago.start.sh
  fastify.addHook('onRequest', async (request, reply) => {
    let token = request.headers["admin_galago_token"]
    if (options.adminGalagoToken==="" || options.adminGalagoToken===token) {
      // all good
    } else {
      reply.code(401).send({
        failure: "😡 Unauthorized",
        success: null
      })
    }
  })

/* [DOC]
### route: `/functions/scale/:function_name/:function_version`

This route allows to scale a version of a function. That means that it will add a new process to a wasm executor.

> How to use it
```bash
function_name="hello"
function_version="003"
curl \
  -X POST "${url_api}/functions/scale/${function_name}/${function_version}"
``` 

[DOC] */  
  fastify.post(`/functions/scale/:function_name/:function_version`, async (request, reply) => {
    //let parameters = request.body
    let functionName = request.params.function_name
    // executor version == function version
    let wasmExecutorVersion = request.params.function_version

    let wasmFileName = `${functionName}_v_${wasmExecutorVersion}.wasm`

    try {
      let wasmChildProcess = addNewWasmFunctionProcessToWasmExecutor({
        wasmFileName: wasmFileName,
        wasmExecutorsMap: options.wasmExecutorsMap
      }).process
  
      wasmChildProcess.send({
        cmd: "load",
        wasmFilePath: `${options.wasmFunctionsFolder}/${wasmFileName}`,
        functionName: functionName
      })
  
      wasmChildProcess.once("message", (message) => {
        if(message.success) { 
          reply.send({
            failure: null,
            success: {
              message: "⭐️ function scaled",
              action: "scaled",
              status: "success"
            }
          })
        } else {
          console.log("😡 error when scaling", message)
          if(message.failure) {
            reply.send({
              failure: message.failure,
              success: null
            })
          } else {
            reply.send({
              failure: "unknown error from wasm worker",
              success: null
            })
          }
        }
      })
    } catch(error) {
      reply.send({
        failure: error.message,
        success: null
      })
    }

    await reply
  })

}

module.exports = scale

/* [DOC]
## wasm.activate.js

- `/functions/activate/:function_name/:function_version`
- /functions/deactivate/:function_name/:function_version

[DOC] */


async function activationManagement (fastify, options) {

  // route protection
  // see galagoctl.config
  // see galago.start.sh
  fastify.addHook('onRequest', async (request, reply) => {
    let token = request.headers["admin_galago_token"]
    if (options.adminGalagoToken==="" || options.adminGalagoToken===token) {
      // all good
    } else {
      reply.code(401).send({
        failure: "😡 Unauthorized",
        success: null
      })
    }
  })


  
/* [DOC]
### route `/functions/activate/:function_name/:function_version`

this route allows to activate a specific version of a wasm executor

> How to use it
```bash
function_name="hello"
version_to_activate="first"
url="${url_api}/functions/activate/${function_name}/${version_to_activate}"

curl -d "{}" \
      -H "Content-Type: application/json" \
      -X POST "${url}"
``` 

#### Reminder about the Wasm Executor

```bash
wasm_executor(
  id: ${function_name}_v_${function_version}.wasm ex helloworld_v_001.wasm
  name: ${function_name}
  defaultVersion: ${function_version}
)
  [
    process 0 with the loaded wasm function 
    process 1 with the loaded wasm function
    ...
  ]
```
- the id of a wasm executor is the name of the wasm file of a specific version of a function
- if N versions of a function exist, you'll get N wasm executors
- for a same function, only one wasm executor is active

[DOC] */
  fastify.post(`/functions/activate/:function_name/:function_version`, async (request, reply) => {
    
    // executor name == function name
    let functionName = request.params.function_name
    // excutor version == function version
    let newDefaultExecutorVersion = request.params.function_version

    // get the current running version
    let currentDefaultExecutorVersion = options.wasmExecutorsVersionsStates.states[functionName]

    if(newDefaultExecutorVersion===currentDefaultExecutorVersion) { // already activated
      return {
        failure: `😡 ${functionName}_v_${newDefaultExecutorVersion} is already activated`, 
        success: null
      }
    } else { // activation
      try {

        let selectedWasmExecutor = options.wasmExecutorsMap.get(`${functionName}_v_${newDefaultExecutorVersion}.wasm`)
        
        if(selectedWasmExecutor) { // the executor exists
  
          selectedWasmExecutor.activateAsDefaultVersion()
  
          // update the state of the active running versions
          options.wasmExecutorsVersionsStates.states[functionName] = newDefaultExecutorVersion
          
          // deactivate the current running version if it exists
          if(currentDefaultExecutorVersion) {
            let executorOfTheFunction = options.wasmExecutorsMap.get(`${functionName}_v_${currentDefaultExecutorVersion}.wasm`)
            if(executorOfTheFunction) {
              executorOfTheFunction.deActivateAsDefaultVersion()
            } else {
              // it could happen if we removed the previous version of the function
              // then the executor doesn't exist
              console.log("🍄 debug: wasm.activate.js | this shouldn't happen 🤔")
              console.log("🍄 debug: wasm.activate.js | the executor of the previous version doesn't exist")
              console.log("🍄 debug: wasm.activate.js | currentDefaultExecutorVersion", currentDefaultExecutorVersion)
            }
          }
  
          return {
            failure: null,
            success: {
              message: `✅ activation of ${functionName}_v_${newDefaultExecutorVersion} done`,
              executor: functionName, 
              defaultVersion: newDefaultExecutorVersion, 
              formerDefaultVersion: currentDefaultExecutorVersion
            }
          }
  
        } else {
          return {
            failure: `😡 ${functionName}_v_${newDefaultExecutorVersion} doesn't exist`, 
            success: null
          }
        }
  
      } catch(error) {
  
        return {
          failure: `${error.message}`, 
          success: null
        }
      }

    } // end if



  })

/* [DOC]
### route `/functions/deactivate/:function_name/:function_version`

[DOC] */
  fastify.post(`/functions/deactivate/:function_name/:function_version`, async (request, reply) => {
    //let parameters = request.body
    // executor name == function name
    let functionName = request.params.function_name
    // excutor version == function version
    let deactivatedVersion = request.params.function_version

    try {

      let selectedWasmExecutor = options.wasmExecutorsMap.get(`${functionName}_v_${deactivatedVersion}.wasm`)
      
      if(selectedWasmExecutor) { // the executor exists

        selectedWasmExecutor.deActivateAsDefaultVersion()

        // update the state of the active running versions
        delete options.wasmExecutorsVersionsStates.states[functionName]
        
        return {
          failure: null,
          success: {
            message: `✅ deactivation of ${functionName}_v_${deactivatedVersion} done`,
            executor: functionName
          }
        }

      } else {
        return {
          failure: `😡 ${functionName}_v_${deactivatedVersion} doesn't exist`, 
          success: null
        }
      }

    } catch(error) {

      return {
        failure: `${error.message}`, 
        success: null
      }
    }

  })

}

module.exports = activationManagement
